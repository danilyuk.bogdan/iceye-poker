# Simple poker

Given two poker hands determine which one wins.

## Installing

git clone https://gitlab.com/danilyuk.bogdan/iceye-poker.git

## Running the tests

`python3 tests.py`


### Usage

````
python3 poker.py AAQTT AAAQQ   ->   Second hand wins!
python3 poker.py AAAQQ AAQTT   ->   First hand wins!
python3 poker.py AAQQT AQTAQ   ->   It's a tie!
````