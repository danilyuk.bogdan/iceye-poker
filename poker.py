import argparse

from consts import RANKS, HANDS_MAPPER, MESSAGES


class ValidationError(BaseException):
    pass


class Card(object):

    def __init__(self, rank):
        '''
        Card object that can be compared using logical operators.
        :param rank: (str) One of '23456789TJQKA' characters.
        '''
        self.rank = self._validate(rank)

    def __ge__(self, other):
        return self.score >= other.score

    def __le__(self, other):
        return self.score <= other.score

    def __ne__(self, other):
        return self.score != other.score

    def __gt__(self, other):
        return self.score > other.score

    def __lt__(self, other):
        return self.score < other.score

    def __eq__(self, other):
        return self.score == other.score

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__, self.rank)

    @property
    def score(self):
        """
        Assign score to each rank:  '2' -> 0,
                                    '3' -> 1,
                                    ...,
                                    'A' -> 12
        :return: (int) score of the rank
        """
        return RANKS.find(self.rank)

    def _validate(self, rank):
        if type(rank) != str:
            raise ValidationError('Unsupported rank type: %s. One character sting expected.' % type(rank))
        if len(rank) != 1:
            raise ValidationError('Unsupported rank format: One character sting expected.')
        if rank not in RANKS:
            raise ValidationError('Unknown card rank. The following ranks are expected: "%s"' % RANKS)
        return rank


class Hand(object):

    def __init__(self, combination):
        '''
        Hand object that defines the value of the combination and can be compared using boolean operators.
        :param combination: (str) five-character string. Each character represents one of card ranks '23456789TJQKA'.
        '''
        self.combination = self._validate(combination)
        self.__card_counts = self._get_count_of_cards()

    def __repr__(self):
        return '%s(%s, %s)' % (self.__class__.__name__, self.combination, self.name)

    def __eq__(self, other):
        return (self.cards == other.cards) and (self.counts == other.counts)

    def __gt__(self, other):
        if self.counts == other.counts:
            return self.cards > other.cards
        return self.counts > other.counts

    def __lt__(self, other):
        if self.counts == other.counts:
            return self.cards < other.cards
        return self.counts < other.counts

    def __ge__(self, other):
        if self.counts == other.counts:
            return self.cards >= other.cards
        return self.counts >= other.counts

    def __le__(self, other):
        if self.counts == other.counts:
            return self.cards <= other.cards
        return self.counts <= other.counts

    def __ne__(self, other):
        return (self.cards != other.cards) or (self.counts != other.counts)

    @property
    def cards(self):
        _, cards = zip(*self.__card_counts)
        return cards

    @property
    def counts(self):
        counts, _ = zip(*self.__card_counts)
        return counts

    @property
    def name(self):
        return HANDS_MAPPER[self.counts]

    def _get_count_of_cards(self):
        '''
        Counts number of each card in hand.
        :return: (list of tuples) Return list of count-card pairs in descending order of count,
                 i.e. AAQAQ -> [(3, Card(A)), (2, Card(Q))]
        '''
        unique_ranks = set(self.combination)
        counts_of_cards = sorted([(self.combination.count(rank), Card(rank)) for rank in unique_ranks], reverse=True)
        return counts_of_cards

    def _validate(self, combination):
        if type(combination) != str:
            raise ValidationError('Unsupported type: %s. Sting expected.' % type(combination))
        if len(combination) != 5:
            raise ValidationError('Unsupported combination format: Five characters sting expected.')
        return combination


def play_poker(hand1, hand2):
    hand1, hand2 = map(Hand, (hand1, hand2))
    if hand1 > hand2:
        print(MESSAGES.WIN1)
    elif hand1 < hand2:
        print(MESSAGES.WIN2)
    else:
        print(MESSAGES.TIE)


if __name__ == '__main__':
    from os import sys, path

    sys.path.append(path.dirname(path.abspath(__file__)))
    parser = argparse.ArgumentParser()
    parser.add_argument("hand1")
    parser.add_argument("hand2")
    args = parser.parse_args()
    play_poker(args.hand1, args.hand2)