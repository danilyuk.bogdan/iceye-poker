RANKS = '23456789TJQKA'


class COMBINATIONS:
    HIGH_CARD = 'High card'
    PAIR = 'Pair'
    TWO_PAIRS = 'Two pairs'
    TRIPLES = 'Triples'
    FULL_HOUSE = 'Full house'
    FOUR_OF_KIND = 'Four of a kind'


HANDS_MAPPER = {
    (1, 1, 1, 1, 1): COMBINATIONS.HIGH_CARD,
    (2, 1, 1, 1): COMBINATIONS.PAIR,
    (2, 2, 1): COMBINATIONS.TWO_PAIRS,
    (3, 1, 1): COMBINATIONS.TRIPLES,
    (3, 2): COMBINATIONS.FULL_HOUSE,
    (4, 1): COMBINATIONS.FOUR_OF_KIND
}


class MESSAGES:
    TIE = "It's a tie!"
    WIN1 = "First hand wins!"
    WIN2 = "Second hand wins!"
