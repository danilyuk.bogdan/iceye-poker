import io
import unittest
import unittest.mock

from consts import COMBINATIONS, MESSAGES
from poker import Card, Hand, ValidationError, play_poker


class CardTest(unittest.TestCase):

    def test_compare_cards_greate(self):
        self.assertTrue(Card('A') > Card('K'))
        self.assertTrue(Card('T') > Card('9'))
        self.assertFalse(Card('2') > Card('7'))
        self.assertFalse(Card('T') > Card('A'))

    def test_compare_cards_less(self):
        self.assertTrue(Card('2') < Card('T'))
        self.assertTrue(Card('K') < Card('A'))
        self.assertTrue(Card('3') < Card('9'))
        self.assertFalse(Card('A') < Card('K'))
        self.assertFalse(Card('T') < Card('8'))

    def test_compare_cards_equal(self):
        self.assertTrue(Card('T') == Card('T'))
        self.assertTrue(Card('K') == Card('K'))
        self.assertTrue(Card('9') == Card('9'))
        self.assertFalse(Card('A') == Card('K'))
        self.assertFalse(Card('T') == Card('8'))

    def test_validation(self):
        self.assertRaises(ValidationError, Card, '')
        self.assertRaises(ValidationError, Card, 'KK')
        self.assertRaises(ValidationError, Card, 'Z')
        self.assertRaises(ValidationError, Card, 'WW')
        self.assertRaises(ValidationError, Card, 1)


class HandTest(unittest.TestCase):

    def test_hand_parsing(self):
        self.assertEqual(Hand('AAAQQ').name, COMBINATIONS.FULL_HOUSE)
        self.assertEqual(Hand('Q53Q4').name, COMBINATIONS.PAIR)
        self.assertEqual(Hand('53888').name, COMBINATIONS.TRIPLES)
        self.assertEqual(Hand('33337').name, COMBINATIONS.FOUR_OF_KIND)
        self.assertEqual(Hand('22333').name, COMBINATIONS.FULL_HOUSE)
        self.assertEqual(Hand('33389').name, COMBINATIONS.TRIPLES)
        self.assertEqual(Hand('44223').name, COMBINATIONS.TWO_PAIRS)
        self.assertEqual(Hand('22456').name, COMBINATIONS.PAIR)
        self.assertEqual(Hand('99977').name, COMBINATIONS.FULL_HOUSE)
        self.assertEqual(Hand('99922').name, COMBINATIONS.FULL_HOUSE)
        self.assertEqual(Hand('9922A').name, COMBINATIONS.TWO_PAIRS)
        self.assertEqual(Hand('2579A').name, COMBINATIONS.HIGH_CARD)
        self.assertEqual(Hand('5678K').name, COMBINATIONS.HIGH_CARD)

    def test_compare_hand_greate(self):
        self.assertTrue(Hand('AAAQQ') > Hand('QQQAA'))
        self.assertTrue(Hand('Q53Q4') > Hand('53QQ2'))
        self.assertTrue(Hand('53888') > Hand('88375'))
        self.assertTrue(Hand('33337') > Hand('QQAAA'))

        self.assertFalse(Hand('AA558') > Hand('22333'))
        self.assertFalse(Hand('AAKK4') > Hand('33389'))
        self.assertFalse(Hand('AA892') > Hand('44223'))
        self.assertFalse(Hand('AKQJT') > Hand('22456'))

    def test_compare_hand_less(self):
        self.assertTrue(Hand('QQQAA') < Hand('AAAQQ'))
        self.assertTrue(Hand('53QQ2') < Hand('Q53Q4'))
        self.assertTrue(Hand('88375') < Hand('53888'))
        self.assertTrue(Hand('QQAAA') < Hand('33337'))

        self.assertFalse(Hand('99975') < Hand('99965'))
        self.assertFalse(Hand('99975') < Hand('99974'))
        self.assertFalse(Hand('99752') < Hand('99652'))
        self.assertFalse(Hand('99752') < Hand('99742'))

    def test_compare_hand_equal(self):
        self.assertTrue(Hand('AAAQQ') == Hand('QQAAA'))
        self.assertTrue(Hand('53QQ2') == Hand('Q53Q2'))
        self.assertTrue(Hand('53888') == Hand('88385'))
        self.assertTrue(Hand('QQAAA') == Hand('AAAQQ'))


class PokerAppTest(unittest.TestCase):

    @unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
    def assert_stdout(self, hand1, hand2, expected_output, mock_stdout):
        play_poker(hand1, hand2)
        self.assertEqual(mock_stdout.getvalue().strip(), expected_output)

    def test_tie(self):
        self.assert_stdout('AAAQQ', 'QQAAA', MESSAGES.TIE)
        self.assert_stdout('53QQ2', 'Q53Q2', MESSAGES.TIE)
        self.assert_stdout('53888', '88385', MESSAGES.TIE)
        self.assert_stdout('QQAAA', 'AAAQQ', MESSAGES.TIE)
        self.assert_stdout('Q53Q2', '53QQ2', MESSAGES.TIE)
        self.assert_stdout('88385', '53888', MESSAGES.TIE)

    def test_first_hand_wins(self):
        self.assert_stdout('AAAQQ', 'QQQAA', MESSAGES.WIN1)
        self.assert_stdout('Q53Q4', '53QQ2', MESSAGES.WIN1)
        self.assert_stdout('53888', '88375', MESSAGES.WIN1)
        self.assert_stdout('33337', 'QQAAA', MESSAGES.WIN1)
        self.assert_stdout('22333', 'AAA58', MESSAGES.WIN1)
        self.assert_stdout('33389', 'AAKK4', MESSAGES.WIN1)
        self.assert_stdout('44223', 'AA892', MESSAGES.WIN1)
        self.assert_stdout('22456', 'AKQJT', MESSAGES.WIN1)
        self.assert_stdout('99977', '77799', MESSAGES.WIN1)
        self.assert_stdout('99922', '88866', MESSAGES.WIN1)
        self.assert_stdout('9922A', '9922K', MESSAGES.WIN1)
        self.assert_stdout('99975', '99965', MESSAGES.WIN1)
        self.assert_stdout('99975', '99974', MESSAGES.WIN1)
        self.assert_stdout('99752', '99652', MESSAGES.WIN1)
        self.assert_stdout('99752', '99742', MESSAGES.WIN1)
        self.assert_stdout('99753', '99752', MESSAGES.WIN1)

    def test_second_hand_wins(self):
        self.assert_stdout('QQQAA', 'AAAQQ', MESSAGES.WIN2)
        self.assert_stdout('53QQ2', 'Q53Q4', MESSAGES.WIN2)
        self.assert_stdout('88375', '53888', MESSAGES.WIN2)
        self.assert_stdout('QQAAA', '33337', MESSAGES.WIN2)
        self.assert_stdout('AAA58', '22333', MESSAGES.WIN2)
        self.assert_stdout('AAKK4', '33389', MESSAGES.WIN2)
        self.assert_stdout('AA892', '44223', MESSAGES.WIN2)
        self.assert_stdout('AKQJT', '22456', MESSAGES.WIN2)
        self.assert_stdout('77799', '99977', MESSAGES.WIN2)
        self.assert_stdout('88866', '99922', MESSAGES.WIN2)
        self.assert_stdout('9922K', '9922A', MESSAGES.WIN2)
        self.assert_stdout('99965', '99975', MESSAGES.WIN2)
        self.assert_stdout('99974', '99975', MESSAGES.WIN2)
        self.assert_stdout('99652', '99752', MESSAGES.WIN2)
        self.assert_stdout('99742', '99752', MESSAGES.WIN2)
        self.assert_stdout('99752', '99753', MESSAGES.WIN2)


if __name__ == '__main__':
    unittest.main()
